import UIKit

class ViewControllerFactory {
    static let listLocator = ListServiceLocator()
    
    class func viewController(type: ViewFactoryType) -> UIViewController {
        switch type {
        case .list:
            let presenter = ListPresenter(
                listUseCase: listLocator.popular,
                resultViewModelMapper: listLocator.listViewModelMapper,
                listErrorViewModelMapper: listLocator.listErrorViewModelMapper
            )
            let viewController = ListViewController(
                presenter: presenter,
                delegate: ListViewDelegate(),
                datasource: ListViewDatasource(),
                listView: ListView()
            )
            return viewController
        }
    }
}
