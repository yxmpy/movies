import Foundation

enum DomainNetworkConstants {
    static let TheMovieDbBaseUrl = "https://api.themoviedb.org"
    static let ImageDbBaseUrl = "http://image.tmdb.org/t/p/w500"
}
