import Foundation

class DomainNetworkBase<T> {
    var baseURL: String {
        return DomainNetworkConstants.TheMovieDbBaseUrl
    }
    
    func url(for _: T, apiKey _: String, argument _: String? = nil) -> String {
        fatalError("Must override")
    }
}
