import Alamofire
import Foundation

class AlamofireServiceLocator {
    private var urlConfiguration: URLSessionConfiguration {
        return AlamofireURLConfiguration(urlConfiguration: URLSessionConfiguration.default).urlConfiguration
    }
    
    private var trustPolicies: AlamofirePolicyManager {
        return AlamofirePolicyManager(policies: [:])
    }
    
    var alamofireSession: AlamofireSession {
        return AlamofireSession(urlConfiguration: urlConfiguration, trustPolicies: trustPolicies)
    }
}
