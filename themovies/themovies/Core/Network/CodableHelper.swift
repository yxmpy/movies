import Foundation

class CodableHelper {
    let decoder = JSONDecoder()
    let encoder = JSONEncoder()
    
    func decodeNetworkObject<D: Decodable>(object: Data) throws -> D {
        do {
            let target = try decoder.decode(D.self, from: object)
            return target
        } catch {
            throw error
        }
    }
    
    func generateErrorFor(object: Data? = Data()) -> ErrorEntity {
        if
            let data = object,
            let error = try? decoder.decode(ErrorEntity.self, from: data) {
            return error
        } else {
            return defaultError()
        }
    }
    
    private func defaultError() -> ErrorEntity {
        return ErrorEntity(response: "false", error: "Unexpected Error")
    }
}
