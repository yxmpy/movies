enum Endpoint {
    enum List: String {
        case popular = "/3/movie/popular?api_key=%@"
        case topRated = "/3/movie/top_rated?api_key=%@"
    }
}
