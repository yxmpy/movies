import Alamofire

class AlamofireSession {
    let manager: SessionManager
    let urlConfiguration: URLSessionConfiguration
    let trustPolicies: ServerTrustPolicyManager
    
    init(urlConfiguration: URLSessionConfiguration, trustPolicies: AlamofirePolicyManager) {
        self.urlConfiguration = urlConfiguration
        self.trustPolicies = trustPolicies
        manager = Alamofire.SessionManager(
            configuration: urlConfiguration,
            serverTrustPolicyManager: trustPolicies
        )
    }
    
    func execute(url: String, method: HTTPMethod = .get, onCompletion: @escaping (DefaultDataResponse) -> Void) {
        manager.request(url, method: method).response(completionHandler: { response in
            onCompletion(response)
        })
    }
}
