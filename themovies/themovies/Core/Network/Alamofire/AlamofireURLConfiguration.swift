import Foundation

class AlamofireURLConfiguration {
    let urlConfiguration: URLSessionConfiguration
    
    init(urlConfiguration: URLSessionConfiguration) {
        self.urlConfiguration = urlConfiguration
        prepare()
    }
    
    private func prepare() {
        urlConfiguration.urlCache = nil
        urlConfiguration.requestCachePolicy = .reloadIgnoringLocalCacheData
        urlConfiguration.httpAdditionalHeaders = nil
    }
}
