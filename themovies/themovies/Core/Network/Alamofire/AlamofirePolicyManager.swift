import Alamofire

class AlamofirePolicyManager: ServerTrustPolicyManager {
    override func serverTrustPolicy(forHost _: String) -> ServerTrustPolicy? {
        return .disableEvaluation
    }
}
