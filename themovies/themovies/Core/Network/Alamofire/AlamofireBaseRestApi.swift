import Foundation

class AlamofireBaseRestApi<T, D>: AlamofireRestApi {
    var domainNetwork: DomainNetworkBase<T>
    var alamofireSession: AlamofireSession
    var codableHelper: CodableHelper
    
    // swiftlint:disable next force_cast
    var network: D {
        return domainNetwork as! D
    }
    
    required init(
        domainNetwork: DomainNetworkBase<T>,
        alamofireSession: AlamofireSession,
        codableHelper: CodableHelper
        ) {
        self.domainNetwork = domainNetwork
        self.alamofireSession = alamofireSession
        self.codableHelper = codableHelper
    }
}
