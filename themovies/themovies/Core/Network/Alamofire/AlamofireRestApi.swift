protocol AlamofireRestApi {
    // swiftlint:disable next type_name
    associatedtype T
    var domainNetwork: DomainNetworkBase<T> { get set }
    var alamofireSession: AlamofireSession { get set }
    var codableHelper: CodableHelper { get set }
    
    init(domainNetwork: DomainNetworkBase<T>, alamofireSession: AlamofireSession, codableHelper: CodableHelper)
}
