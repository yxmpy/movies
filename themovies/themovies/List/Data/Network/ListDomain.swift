import Foundation

class ListDomain: DomainNetworkBase<Endpoint.List> {
    override func url(for endpoint: Endpoint.List, apiKey: String, argument: String? = nil) -> String {
        if let argument = argument {
            return baseURL + String(format: endpoint.rawValue, apiKey, argument)
        } else {
            return baseURL + String(format: endpoint.rawValue, apiKey)
        }
    }
}
