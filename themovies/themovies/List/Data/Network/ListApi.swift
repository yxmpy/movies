import Foundation

protocol ListApi {
    
    func popular(completionHandler: @escaping (ListResultsEntity?, ErrorEntity?) -> Void)
    
    func topRated(completionHandler: @escaping (ListResultsEntity?, ErrorEntity?) -> Void)
}
