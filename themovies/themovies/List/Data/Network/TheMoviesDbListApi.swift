import Alamofire
import Foundation

class TheMoviesDbListApi: AlamofireBaseRestApi<Endpoint.List, ListDomain>, ListApi {
    
    func popular(completionHandler: @escaping (ListResultsEntity?, ErrorEntity?) -> Void) {
        let url = network.url(for: .popular, apiKey: Constants.apiKey, argument: nil)
        alamofireSession.execute(url: url) { response in
            if
                let data = response.data,
                let entity: ListResultsEntity = try? self.codableHelper.decodeNetworkObject(object: data) {
                completionHandler(entity, nil)
            } else {
                completionHandler(nil, self.codableHelper.generateErrorFor(object: response.data))
            }
        }
    }
    
    func topRated(completionHandler: @escaping (ListResultsEntity?, ErrorEntity?) -> Void) {
        let url = network.url(for: .topRated, apiKey: Constants.apiKey, argument: nil)
        alamofireSession.execute(url: url) { response in
            if
                let data = response.data,
                let entity: ListResultsEntity = try? self.codableHelper.decodeNetworkObject(object: data) {
                completionHandler(entity, nil)
            } else {
                completionHandler(nil, self.codableHelper.generateErrorFor(object: response.data))
            }
        }
    }
}
