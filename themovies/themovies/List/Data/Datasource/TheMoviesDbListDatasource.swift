import Foundation

class TheMoviesDbListDatasource: ListDatasource {
    private let api: ListApi
    
    init(api: ListApi) {
        self.api = api
    }
    
    func popular(completionHandler: @escaping ([ListResultEntity]?, ErrorEntity?) -> Void) {
        api.popular { results, error in
            if let results = results {
                completionHandler(results.results, nil)
            } else {
                completionHandler(nil, error)
            }
        }
    }
    
    func topRated(completionHandler: @escaping ([ListResultEntity]?, ErrorEntity?) -> Void) {
        api.topRated { results, error in
            if let results = results {
                completionHandler(results.results, nil)
            } else {
                completionHandler(nil, error)
            }
        }
    }
}
