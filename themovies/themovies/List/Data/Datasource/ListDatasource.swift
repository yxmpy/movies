import Foundation

protocol ListDatasource {
    
    func popular(completionHandler: @escaping ([ListResultEntity]?, ErrorEntity?) -> Void)
    
    func topRated(completionHandler: @escaping ([ListResultEntity]?, ErrorEntity?) -> Void)
}
