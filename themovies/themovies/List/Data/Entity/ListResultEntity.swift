import Foundation

struct ListResultEntity: Codable {
    let voteCount: Float
    let id: Float
    let video: Bool
    let voteAverage: Float
    let title: String
    let popularity: Float
    let posterPath: String
    let originalLanguage: String
    let originalTitle: String
    let backdropPath: String
    let adult: Bool
    let overview: String
    let releaseDate: String
    
    private enum CodingKeys: String, CodingKey {
        case voteCount = "vote_count"
        case id
        case video
        case voteAverage = "vote_average"
        case title
        case popularity
        case posterPath = "poster_path"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case backdropPath = "backdrop_path"
        case adult
        case overview
        case releaseDate = "release_date"
    }
}
