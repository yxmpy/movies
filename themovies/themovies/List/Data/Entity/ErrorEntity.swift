import Foundation

struct ErrorEntity: Codable {
    let response: String
    let error: String
    
    private enum CodingKeys: String, CodingKey {
        case response = "Response"
        case error = "Error"
    }
}
