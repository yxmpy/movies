import Foundation

struct ListResultsEntity: Codable {
    let page: Float
    let totalResults: Float
    let totalPages: Float
    let results: [ListResultEntity]
    
    private enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
}
