import Foundation

class ListServiceLocator {
    private let alamofireLocator = AlamofireServiceLocator()
    
    // MARK: Network
    
    private var domainNetwork: DomainNetworkBase<Endpoint.List> {
        return ListDomain()
    }
    
    private var codableHelper: CodableHelper {
        return CodableHelper()
    }
    
    private var listApi: ListApi {
        return TheMoviesDbListApi(
            domainNetwork: domainNetwork,
            alamofireSession: alamofireLocator.alamofireSession,
            codableHelper: codableHelper
        )
    }
    
    private var listDatasource: ListDatasource {
        return TheMoviesDbListDatasource(api: listApi)
    }
    
    // MARK: Mapper
    
    private var listErrorMapper: Mapper<ListError, ErrorEntity> {
        return ListErrorMapper()
    }
    
    private var listResultMapper: Mapper<ListResult, ListResultEntity> {
        return ListResultMapper()
    }
    
    var listViewModelMapper: Mapper<ListViewModel, ListResult> {
        return ListViewModelMapper()
    }
    
    var listErrorViewModelMapper: Mapper<ListErrorViewModel, ListError> {
        return ListErrorViewModelMapper()
    }
    
    // MARK: Domain
    
    private var listRepository: ListRepository {
        return TheMovieDbListRepository(
            datasource: listDatasource,
            resultMapper: listResultMapper,
            errorMapper: listErrorMapper
        )
    }
    
    var popular: ListUseCase {
        return ListUseCase(listRepository: listRepository)
    }
    
    var topRated: ListUseCase {
        return ListUseCase(listRepository: listRepository)
    }
}
