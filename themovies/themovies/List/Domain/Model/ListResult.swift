import Foundation

struct ListResult {
    let voteCount: Float
    let id: Float
    let video: Bool
    let voteAverage: Float
    let title: String
    let popularity: Float
    let posterPath: String
    let originalLanguage: String
    let originalTitle: String
    let backdropPath: String
    let adult: Bool
    let overview: String
    let releaseDate: String
}
