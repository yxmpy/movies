import Foundation

struct ListUseCase {
    private let listRepository: ListRepository
    
    init(listRepository: ListRepository) {
        self.listRepository = listRepository
    }
    
    func popular(completionHandler: @escaping ([ListResult]?, ListError?) -> Void) {
        listRepository.popular { results, error in
            completionHandler(results, error)
        }
    }
    
    func topRated(completionHandler: @escaping ([ListResult]?, ListError?) -> Void) {
        listRepository.topRated { results, error in
            completionHandler(results, error)
        }
    }
}
