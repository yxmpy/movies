import Foundation

class TheMovieDbListRepository: ListRepository {
    private let datasource: ListDatasource
    private let resultMapper: Mapper<ListResult, ListResultEntity>
    private let errorMapper: Mapper<ListError, ErrorEntity>
    
    init(
        datasource: ListDatasource,
        resultMapper: Mapper<ListResult, ListResultEntity>,
        errorMapper: Mapper<ListError, ErrorEntity>
        ) {
        self.datasource = datasource
        self.resultMapper = resultMapper
        self.errorMapper = errorMapper
    }
    
    func popular(completionHandler: @escaping ([ListResult]?, ListError?) -> Void) {
        datasource.popular { results, error in
            if let results = results {
                completionHandler(self.resultMapper.reverseMap(values: results), nil)
            } else if let error = error {
                completionHandler(nil, self.errorMapper.reverseMap(value: error))
            }
        }
    }
    
    func topRated(completionHandler: @escaping ([ListResult]?, ListError?) -> Void) {
        datasource.topRated { results, error in
            if let results = results {
                completionHandler(self.resultMapper.reverseMap(values: results), nil)
            } else if let error = error {
                completionHandler(nil, self.errorMapper.reverseMap(value: error))
            }
        }
    }
}
