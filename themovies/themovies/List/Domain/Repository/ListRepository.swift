import Foundation

protocol ListRepository {
    
    func popular(completionHandler: @escaping ([ListResult]?, ListError?) -> Void)
    
    func topRated(completionHandler: @escaping ([ListResult]?, ListError?) -> Void)
}
