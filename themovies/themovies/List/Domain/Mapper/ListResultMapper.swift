import Foundation

class ListResultMapper: Mapper<ListResult, ListResultEntity> {
    override func reverseMap(value: ListResultEntity) -> ListResult {
        let listResult = ListResult(
            voteCount: value.voteCount,
            id: value.id,
            video: value.video,
            voteAverage: value.voteAverage,
            title: value.title,
            popularity: value.popularity,
            posterPath: value.posterPath,
            originalLanguage: value.originalLanguage,
            originalTitle: value.originalTitle,
            backdropPath: value.backdropPath,
            adult: value.adult,
            overview: value.overview,
            releaseDate: value.releaseDate
        )
        return listResult
    }
}
