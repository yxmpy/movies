import Foundation

class ListErrorMapper: Mapper<ListError, ErrorEntity> {
    override func reverseMap(value: ErrorEntity) -> ListError {
        let listError = ListError(description: value.error)
        return listError
    }
}
