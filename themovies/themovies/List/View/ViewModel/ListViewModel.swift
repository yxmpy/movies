import Foundation

struct ListViewModel {
    let title: NSAttributedString
    let identifier: Float
    let imageUrl: URL?
}
