import Foundation

struct ListErrorViewModel {
    let title: String
    let error: String
}
