import AlamofireImage
import UIKit

class ListResultCell: UITableViewCell {
    @IBOutlet var resultImage: UIImageView!
    @IBOutlet var resultText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepare()
    }
    
    private func prepare() {
        prepareResultImage()
        prepareResultText()
    }
    
    private func prepareResultImage() {
        resultImage.contentMode = .scaleAspectFit
    }
    
    private func prepareResultText() {
        resultText.numberOfLines = 0
        resultText.textAlignment = .left
    }
    
    func configure(text: NSAttributedString, imageUrl: URL?) {
        if let imageUrl = imageUrl {
            var urlstring = DomainNetworkConstants.ImageDbBaseUrl
            urlstring += imageUrl.absoluteString
            let urlRequest = URLRequest(url: URL(string: urlstring)!)
            resultImage.af_setImage(withURLRequest: urlRequest, placeholderImage: #imageLiteral(resourceName: "movies"))
        }
        resultText.attributedText = text
    }
}
