import Foundation

class ListErrorViewModelMapper: Mapper<ListErrorViewModel, ListError> {
    override func reverseMap(value: ListError) -> ListErrorViewModel {
        let listErrorViewModel = ListErrorViewModel(
            title: "Ops!",
            error: value.description
        )
        return listErrorViewModel
    }
}
