import Foundation
import UIKit

class ListViewModelMapper: Mapper<ListViewModel, ListResult> {
    override func reverseMap(value: ListResult) -> ListViewModel {
        let listViewModel = ListViewModel(
            title: generateText(with: value),
            identifier: value.id,
            imageUrl: URL(string: value.posterPath)
        )
        return listViewModel
    }
    
    private func generateText(with value: ListResult) -> NSAttributedString {
        let boldAtttributes: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize: 14)]
        let regularAttributes: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 12)]
        let italicAttributes: [NSAttributedString.Key: Any] = [.font: UIFont.italicSystemFont(ofSize: 10)]
        let boldText = NSMutableAttributedString(string: value.title, attributes: boldAtttributes)
        let regularText = NSAttributedString(string: " (\(value.voteAverage)) (\(value.originalLanguage))\n\(value.releaseDate)\n\n", attributes: regularAttributes)
        let italicText = NSAttributedString(string: " \(value.overview)", attributes: italicAttributes)
        boldText.append(regularText)
        boldText.append(italicText)
        return boldText
    }
}
