import Foundation
import UIKit

class ListViewDatasource: NSObject {
    weak var viewController: ListViewController?
    var results: [ListViewModel] = []
}

extension ListViewDatasource: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return results.count
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let resultCell = tableView.dequeueReusableCell(
            withIdentifier: ListViewConstants.ListReuseCell.identifier,
            for: indexPath
            ) as! ListResultCell
        resultCell.configure(text: results[indexPath.row].title, imageUrl: results[indexPath.row].imageUrl)
        return resultCell
    }
    
    // swiftlint:enable force_cast
}
