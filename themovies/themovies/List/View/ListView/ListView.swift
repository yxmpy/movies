import UIKit

class ListView: NSObject {
    weak var viewController: ListViewController?
    var datasource: ListViewDatasource? {
        return viewController?.tableView.dataSource as? ListViewDatasource
    }
}

extension ListView: ListViewContract {
    func show(results: [ListViewModel]) {
        datasource?.results = results
        viewController?.tableView.reloadData()
    }
    
    func show(error: ListErrorViewModel) {
        let alert = UIAlertController(
            title: error.title,
            message: error.error,
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: nil))
        viewController?.present(alert, animated: true, completion: nil)
    }
}
