import Foundation

protocol ListPresenterContract: class {
    var view: ListViewContract? { get set }
    func attach(view: ListViewContract)
    func dettach()
    func getPopular()
    func getTopRated()
}
