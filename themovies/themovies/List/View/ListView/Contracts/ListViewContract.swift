import Foundation

protocol ListViewContract: class {
    var viewController: ListViewController? { get set }
    func show(results: [ListViewModel])
    func show(error: ListErrorViewModel)
}
