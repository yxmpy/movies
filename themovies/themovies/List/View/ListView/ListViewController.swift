import UIKit

class ListViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    
    private var presenter: ListPresenterContract?
    // swiftlint:disable next weak_delegate
    private var delegate: ListViewDelegate?
    private var datasource: ListViewDatasource?
    private var listView: ListViewContract?
    private var isPopularList: Bool = true
    
    convenience init(
        presenter: ListPresenterContract,
        delegate: ListViewDelegate,
        datasource: ListViewDatasource,
        listView: ListViewContract
        ) {
        self.init()
        self.listView = listView
        listView.viewController = self
        self.presenter = presenter
        presenter.attach(view: listView)
        self.delegate = delegate
        delegate.viewController = self
        self.datasource = datasource
        datasource.viewController = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = ListViewConstants.View.titlePopular
        prepare()
        presenter?.getPopular()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func prepare() {
        prepareTableView()
        prepareSortButton()
    }
    
    private func prepareTableView() {
        tableView.delegate = delegate
        tableView.dataSource = datasource
        tableView.register(
            ListViewConstants.ListReuseCell.nib,
            forCellReuseIdentifier: ListViewConstants.ListReuseCell.identifier
        )
    }
    
    private func prepareSortButton() {
        let sortBarButtonItem = UIBarButtonItem(title: "Order By", style: .done, target: self, action: #selector(sort))
        navigationItem.rightBarButtonItem = sortBarButtonItem
    }
    
    @objc private func sort() {
        if isPopularList {
            presenter?.getTopRated()
            title = ListViewConstants.View.titleTopRated
            isPopularList = false
        } else {
            presenter?.getPopular()
            title = ListViewConstants.View.titlePopular
            isPopularList = true
        }
    }
}
