import Foundation
import UIKit

class ListViewDelegate: NSObject {
    weak var viewController: ListViewController?
    var thereIsCellTapped = false
    var selectedRowIndex = -1
}

extension ListViewDelegate: UITableViewDelegate {
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == selectedRowIndex && thereIsCellTapped {
            return ListViewConstants.ListReuseCell.detailHeight
        }
        return ListViewConstants.ListReuseCell.height
    }
    
    func tableView(_: UITableView, estimatedHeightForRowAt _: IndexPath) -> CGFloat {
        return ListViewConstants.ListReuseCell.estimatedHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedRowIndex != indexPath.row {
            thereIsCellTapped = true
            selectedRowIndex = indexPath.row
        } else {
            thereIsCellTapped = false
            selectedRowIndex = -1
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
