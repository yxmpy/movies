import Foundation

class ListPresenter: ListPresenterContract {
    var view: ListViewContract?
    
    private let listUseCase: ListUseCase
    private let resultViewModelMapper: Mapper<ListViewModel, ListResult>
    private let listErrorViewModelMapper: Mapper<ListErrorViewModel, ListError>
    
    init(
        listUseCase: ListUseCase,
        resultViewModelMapper: Mapper<ListViewModel, ListResult>,
        listErrorViewModelMapper: Mapper<ListErrorViewModel, ListError>
        ) {
        self.listUseCase = listUseCase
        self.resultViewModelMapper = resultViewModelMapper
        self.listErrorViewModelMapper = listErrorViewModelMapper
    }
    
    func attach(view: ListViewContract) {
        self.view = view
    }
    
    func dettach() {
        view = nil
    }
    
    func getPopular() {
        listUseCase.popular { results, error in
            if let results = results {
                self.view?.show(results: self.resultViewModelMapper.reverseMap(values: results))
            } else if let error = error {
                self.view?.show(error: self.listErrorViewModelMapper.reverseMap(value: error))
            }
        }
    }
    
    func getTopRated() {
        listUseCase.topRated { results, error in
            if let results = results {
                self.view?.show(results: self.resultViewModelMapper.reverseMap(values: results))
            } else if let error = error {
                self.view?.show(error: self.listErrorViewModelMapper.reverseMap(value: error))
            }
        }
    }
}
