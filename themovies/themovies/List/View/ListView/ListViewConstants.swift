import UIKit

enum ListViewConstants {
    enum View {
        static let titleDefault = "Movies"
        static let titlePopular = "Popular"
        static let titleTopRated = "Top Rated"
    }
    
    enum ListReuseCell {
        static let nib: UINib = UINib(nibName: "ListResultCell", bundle: nil)
        static let identifier = "ListResultCellReuseIdentifier"
        static let height: CGFloat = 100
        static let detailHeight: CGFloat = 200
        static let estimatedHeight: CGFloat = 90
        static let estimatedDetailHeight: CGFloat = 180
    }
}
